﻿using ConsoleApp20.Helpers;
using ConsoleApp20.Models;
using MyApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp20
{
    public static class Order
    {



       










        public static void Basket()
        {

            char cn = '0';
            while (cn != '2')
            {

                while (true)
                {
                    ProgramWarnings.InfoText();
                    Console.Write("Your basket: ");
                    foreach (var p in Program.basket)
                    {
                        Console.Write(p.Product.Name + " ");
                    }
                    Console.WriteLine();
                    Console.WriteLine("1. Add more products to the basket.");
                    Console.WriteLine("2. Confirm order");
                    Console.Write("Your choice: ");

                    cn = Console.ReadKey().KeyChar;

                    if (cn == '1')
                    {
                        Console.WriteLine();
                        Console.Write("Input the name of the product: ");
                        string prname = Console.ReadLine();
                        Product pr = Program.products.Where(p => p.Name == prname).FirstOrDefault();
                        if (pr == null)
                        {
                            ProgramWarnings.Warnings(1);
                            continue;
                        }

                        BasketProduct prd = new BasketProduct();
                        prd.Product = pr;
                        Program.basket.Enqueue(prd);
                    }
                    break;
                }
            }
            CheckOut();

        }

      public  static void CheckOut()
        {
            List<Product> discountOne = new List<Product>();
            List<Product> discountTwo = new List<Product>();
            int total = 0;

            ProgramWarnings.InfoText();

            foreach (var p in Program.basket)
            {
                if (p.Product.DiscountName == "2for3")
                {
                    p.HasDiscount = true;
                    discountOne.Add(p.Product);
                }

                if (discountOne.Count == 3)
                {
                    Product pr = discountOne.OrderBy(p => p.Price).FirstOrDefault();
                    p.DiscountPrice = 0;
                    discountOne.Remove(pr);
                    foreach (var prod in discountOne)
                    {
                        total += prod.Price;
                    }
                    total += p.DiscountPrice;
                    discountOne.Clear();
                }

                if (p.Product.DiscountName == "halfprice")
                {
                    p.HasDiscount = true;
                    discountTwo.Add(p.Product);
                }

                if (discountTwo.Count == 2)
                {
                    Product pr = discountTwo.OrderBy(p => p.Price).FirstOrDefault();
                    p.DiscountPrice = pr.Price / 2;
                    discountTwo.Remove(pr);
                    foreach (var prod in discountTwo)
                    {
                        total += prod.Price;
                    }
                    total += p.DiscountPrice;
                    discountTwo.Clear();
                }


                if (p.DiscountPrice == -1 && p.HasDiscount == false)
                {
                    total += p.Product.Price;
                }


            }

            if (discountOne.Count != 0)
            {
                foreach (var d in discountOne)
                {
                    total += d.Price;
                }
            }

            if (discountTwo.Count != 0)
            {
                foreach (var d in discountTwo)
                {
                    total += d.Price;
                }
            }

            int aws = total / 100;
            int remainder = total % 100;
            Console.WriteLine($"{aws} aws and {remainder} c");

            char cn = Console.ReadKey().KeyChar;

            Console.WriteLine("0. OK");

            Program.basket.Clear();
        }
    }
}
