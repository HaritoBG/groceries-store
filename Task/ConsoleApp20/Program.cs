﻿using ConsoleApp20;
using ConsoleApp20.Helpers;
using ConsoleApp20.Models;
using System;

namespace MyApp // Note: actual namespace depends on the project name.
{
    internal class Program
    {
        //Admin Password : 12345678 
       public static List<Product> products = new List<Product>();
        public static Queue<BasketProduct> basket = new Queue<BasketProduct>();




        static void BasicOptions()
        {

            Console.WriteLine("=====");
            Console.WriteLine("Press a number between 0 - 2 to choose an option.");
            Console.WriteLine("0. Leave");
            Console.WriteLine("1. Purchase Products");
            Console.WriteLine("2. Admin Panel");
            Console.WriteLine("=====");

            Console.Write("Your choice: ");

        }



        static void Main(string[] args)
        {
        
            //Test Data
            //Product producter1 = new Product();
            //producter1.Name = "apple";
            //producter1.Price = 50;
            //producter1.DiscountName = "2for3";
            //products.Add(producter1);

            //Product producter2 = new Product();
            //producter2.Name = "banana";
            //producter2.DiscountName = "2for3";
            //producter2.Price = 40;
            //products.Add(producter2);

            //Product producter3 = new Product();
            //producter3.Name = "tomato";
            //producter3.Price = 30;
            //products.Add(producter3);

            //Product producter4 = new Product();
            //producter4.Name = "potato";
            //producter4.DiscountName = "halfprice";
            //producter4.Price = 26;
            //products.Add(producter4);


            char cn = '1';
            while (cn != '0')
            {


                ProgramWarnings.InfoText();
                BasicOptions();


                 cn = Console.ReadKey().KeyChar;

                if (cn < '0' || cn > '2')
                {
                    ProgramWarnings.Warnings(4);
                    continue;
                }

                switch (cn)
                {
                    case '1':
                        {
                            if(products.Count == 0)
                            {
                                ProgramWarnings.Warnings(2); break;
                            }
                            Order.Basket();
                        }
                        break;
                    case '2': AdminOptions.AdminPanel(); break;

                }

            }


            Console.WriteLine();
            Console.WriteLine("Goodbye!");

       
        }

        
    }
}