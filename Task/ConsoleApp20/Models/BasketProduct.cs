﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp20.Models
{
    internal class BasketProduct
    {

        public Product Product { get; set; }


        public bool HasDiscount { get; set; } = false;

        public int DiscountPrice { get; set; } = -1;
    }
}
