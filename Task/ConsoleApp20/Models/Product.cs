﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp20.Models
{
    internal class Product
    {

        public string Name { get; set; }

        public int Price { get; set; }

        public string DiscountName { get; set; }



    }
}
