﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp20.Helpers
{
    public static class ProgramWarnings
    {



        public static void InfoText()
        {
            Console.Clear();
            Console.WriteLine("[---] Groceries shop [---]");
        }




        public static void Warnings(int warningNumber)
        {
            InfoText();
            Console.WriteLine("! Warning !");
            switch (warningNumber)
            {
                case 0: Console.WriteLine("Invalid value for Product's price!"); break;
                case 1: Console.WriteLine("This product does not exist!"); break;
                case 2: Console.WriteLine("There are no products added!"); break;
                case 3: Console.WriteLine("Incorrect Password!"); break;
                case 4: Console.WriteLine("The number you have pressed is not an option!"); break;
                case 5: Console.WriteLine("This product already exists!"); break;



            }

            Console.WriteLine("0. OK");
            Console.ReadKey();
        }
    }
}
