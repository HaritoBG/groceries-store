﻿using ConsoleApp20.Helpers;
using ConsoleApp20.Models;
using MyApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp20
{
    public static class AdminOptions
    {



       public static void AdminPanel()
        {
            Console.WriteLine();
            Console.Write("Input an admin password: ");
            string pass = Console.ReadLine();
            if (pass != "12345678")
            {
                return;
            }
            while (true)
            {
                ProgramWarnings.InfoText();
                Console.WriteLine("<-- Admin Panel -->");
                Console.WriteLine("1. Add a product");
                Console.WriteLine("2. Include a product in discount");
                Console.Write("Your choice: ");

                char cn = Console.ReadKey().KeyChar;

                Console.WriteLine();

                if (cn < '1' || cn > '2')
                {
                    ProgramWarnings.Warnings(4);
                    continue;
                }
                switch (cn)
                {
                    case '1': AddProduct(); break;
                    case '2':
                        {
                            if (Program.products.Count == 0)
                            {

                                ProgramWarnings.Warnings(2);
                                continue;
                            }
                            AddProductDiscount();
                        }
                        break;
                }
                break;
            }

        }


        public static void AddProduct()
        {
            while (true)
            {
                ProgramWarnings.InfoText();
                Console.WriteLine("<-- Adding a new product -->");
                Console.Write("Product's Name: ");
                string name = Console.ReadLine();
                bool checker = false;


                if (Program.products.Where(p => p.Name == name).FirstOrDefault() != null)
                {
                    ProgramWarnings.Warnings(5);
                    continue;
                }
                Console.Write("Product's Price: ");
                string p = Console.ReadLine();
                int price = 0;

                if (p.EndsWith('c'))
                {
                    int index = p.IndexOf('c');
                    p = p.Substring(0, index);

                    bool success = int.TryParse(p, out int number);
                    if (success)
                    {
                        price = number;
                    }
                    else
                    {
                        ProgramWarnings.Warnings(0);
                        continue;
                    }

                    Console.WriteLine();


                    checker = true;




                }

                if (p.EndsWith("aws"))
                {
                    int index = p.IndexOf('a');
                    p = p.Substring(0, index);
                    bool success = int.TryParse(p, out int number);
                    if (success)
                    {
                        price = number * 100;
                    }
                    else
                    {
                        ProgramWarnings.Warnings(0);
                        continue;
                    }

                    Console.WriteLine();


                    checker = true;

                }

                if (checker)
                {

                    Product product = new Product();
                    product.Name = name;
                    product.Price = price;
                    Program.products.Add(product);
                    break;
                }

                ProgramWarnings.Warnings(0);
                continue;
            }

        }


        static void AddProductDiscount()
        {
            while (true)
            {
                ProgramWarnings.InfoText();
                Console.WriteLine("<-- Include a product in discount --> ");
                Console.Write("Input the name of the product: ");
                string name = Console.ReadLine();
                Product pr = Program.products.Where(c => c.Name == name).FirstOrDefault();
                if (pr == null)
                {
                    ProgramWarnings.Warnings(1);
                    continue;
                }

                while (true)
                {
                    ProgramWarnings.InfoText();
                    Console.WriteLine($"Product < {pr.Name} > ");
                    Console.WriteLine("Choose 1 discount option");
                    Console.WriteLine("1. Discount: '2 for 3'");
                    Console.WriteLine("2. Discount: 'buy 1 get 1 half price'");
                    Console.Write("Your choice: ");


                    char cn = Console.ReadKey().KeyChar;

                    if (cn < '1' || cn > '2')
                    {
                        ProgramWarnings.Warnings(4);
                        continue;
                    }

                    switch (cn)
                    {
                        case '1': pr.DiscountName = "2for3"; break;
                        case '2': pr.DiscountName = "halfprice"; break;

                    }
                    break;
                }
                break;
            }

        }

    }
}
